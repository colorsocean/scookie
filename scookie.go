//
// Don't forget to gob.Register() custom types that you use
// as SessionCookie.SessVars
package scookie

import (
	"log"
	"net"
	"net/http"
	"strings"
	"sync"
	"time"

	"github.com/codegangsta/negroni"
	"github.com/gorilla/securecookie"
	"github.com/nbio/httpcontext"
	"github.com/zenazn/goji/web"
)

/****************************************************************
** Middlewares
********/

const reqCtxKey = "scookie.SessionCookie"

func C(c interface{}) *SessionCookie {
	switch c := c.(type) {
	case *http.Request:
		return httpcontext.Get(c, reqCtxKey).(*SessionCookie)
	case *web.C:
		//if c.Env != nil {
		return c.Env[reqCtxKey].(*SessionCookie)
		//}
	case web.C:
		//if c.Env != nil {
		return c.Env[reqCtxKey].(*SessionCookie)
		//}
	}
	return nil
}

func SessionCookieGoji(ops Options) func(c *web.C, h http.Handler) http.Handler {
	return func(c *web.C, h http.Handler) http.Handler {

		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			sc := &SessionCookie{
				options: ops,
			}
			//if c.Env != nil {
			c.Env[reqCtxKey] = sc
			//}
			sc.Read(r)
			whook := &responseWriterHook{sc: sc, w: w}
			h.ServeHTTP(whook, r)
		})
	}
}

func SessionCookieNeg(ops Options) negroni.Handler {
	return negroni.HandlerFunc(func(w http.ResponseWriter, q *http.Request, next http.HandlerFunc) {
		sc := &SessionCookie{
			options: ops,
		}
		httpcontext.Set(q, reqCtxKey, sc)
		sc.Read(q)
		whook := &responseWriterHook{sc: sc, w: w}
		next(whook, q)
	})
}

/****************************************************************
**
********/

type responseWriterHook struct {
	sc   *SessionCookie
	w    http.ResponseWriter
	once sync.Once
}

func (this *responseWriterHook) ensureSet() {
	this.once.Do(func() {
		this.sc.Write(this.w)
	})
}

func (this *responseWriterHook) Header() http.Header {
	return this.w.Header()
}

func (this *responseWriterHook) Write(data []byte) (int, error) {
	this.ensureSet()
	return this.w.Write(data)
}

func (this *responseWriterHook) WriteHeader(status int) {
	this.ensureSet()
	this.w.WriteHeader(status)
}

/****************************************************************
** Session Cookie impl
********/

type SessionCookie struct {
	enc *securecookie.SecureCookie

	data        *sessionCookieData
	dataBackups []*sessionCookieData

	options Options

	host string

	once sync.Once
}

type Options struct {
	SessionLifetime time.Duration
	VisitLifetime   time.Duration

	Name     string
	Domain   string
	Wildcard bool // todo: Implement levels
	Secure   bool
	HashKey  []byte
	BlockKey []byte
}

type sessionCookieData struct {
	SessionToken        string
	SessionTokenCreated time.Time
	SessionTokenRenewed time.Time

	VisitToken        string
	VisitTokenCreated time.Time
	VisitTokenRenewed time.Time

	SessVars map[string]interface{}
}

func (this *SessionCookie) init() {
	this.once.Do(func() {
		this.enc = securecookie.New(this.options.HashKey, this.options.BlockKey)

		if this.options.SessionLifetime == 0 {
			this.options.SessionLifetime = 24 * 30 * time.Hour
		}

		if this.options.VisitLifetime == 0 {
			this.options.VisitLifetime = 1 * time.Hour
		}

		this.data = &sessionCookieData{}

		this.data.SessVars = make(map[string]interface{})

		this.dataBackups = []*sessionCookieData{}

		this.check()
	})
}

func (this *SessionCookie) check() {
	now := time.Now().UTC()

	if !matchUUIDNoDashes.MatchString(this.data.SessionToken) ||
		now.After(this.data.SessionTokenRenewed.Add(this.options.SessionLifetime)) {
		this.data.SessionToken = randomUUIDNoDashes()
		this.data.SessionTokenCreated = now
	}

	if !matchUUIDNoDashes.MatchString(this.data.VisitToken) ||
		now.After(this.data.VisitTokenRenewed.Add(this.options.VisitLifetime)) {
		this.data.VisitToken = randomUUIDNoDashes()
		this.data.VisitTokenCreated = now
	}
}

func (this *SessionCookie) Reset() {
	this.init()

	this.push()

	now := time.Now().UTC()

	this.data.SessionToken = randomUUIDNoDashes()
	this.data.SessionTokenCreated = now

	this.data.VisitToken = randomUUIDNoDashes()
	this.data.VisitTokenCreated = now

	this.data.SessionTokenRenewed = now
	this.data.VisitTokenRenewed = now
}

func (this *SessionCookie) ResetVisit() {
	this.init()

	this.push()

	now := time.Now().UTC()

	this.data.VisitToken = randomUUIDNoDashes()
	this.data.VisitTokenCreated = now
	this.data.VisitTokenRenewed = now
}

func (this *SessionCookie) ResetSession() {
	this.init()

	this.push()

	now := time.Now().UTC()

	this.data.SessionToken = randomUUIDNoDashes()
	this.data.SessionTokenCreated = now
	this.data.SessionTokenRenewed = now
}

func (this *SessionCookie) push() { // todo: Test!
	this.init()

	if this.data != nil && this.data.SessionToken != "" && this.data.VisitToken != "" {
		this.dataBackups = append(this.dataBackups, this.data)
	}
}

func (this *SessionCookie) Pop() { // todo: Test!
	this.init()

	if len(this.dataBackups) > 0 {
		this.data = this.dataBackups[len(this.dataBackups)-1]
		this.dataBackups = this.dataBackups[:len(this.dataBackups)-1]
	}
}

func (this *SessionCookie) SessionToken() string {
	this.init()

	return this.data.SessionToken
}

func (this *SessionCookie) VisitToken() string {
	this.init()

	return this.data.VisitToken
}

func (this *SessionCookie) GetVar(key string) (val interface{}, ok bool) { // todo: Test!
	this.init()

	val, ok = this.data.SessVars[key]

	return
}

func (this *SessionCookie) SetVar(key string, val interface{}) { // todo: Test!
	this.init()

	this.data.SessVars[key] = val
}

func (this *SessionCookie) DelVar(key string) {
	this.init()

	delete(this.data.SessVars, key)
}

func (this *SessionCookie) Read(r *http.Request) {
	this.init()

	this.host = r.Host

	cookie, err := r.Cookie(this.options.Name)
	if err == nil {
		err = this.enc.Decode(this.options.Name, cookie.Value, this.data)
		if err != nil {
			log.Println("Can not decode cookie")
		}
	} else {
		//log.Println("No cookie present")
	}

	this.check()

	this.data.SessionTokenRenewed = time.Now().UTC()
	this.data.VisitTokenRenewed = this.data.SessionTokenRenewed
}

func (this *SessionCookie) Write(w http.ResponseWriter) {
	this.init()

	this.check()

	if encoded, err := this.enc.Encode(this.options.Name, this.data); err == nil {
		age := maxDuration(this.options.SessionLifetime, this.options.VisitLifetime)

		domain := ""
		if this.options.Domain != "" {
			domain = this.options.Domain
		} else {
			_domain := this.host
			portSepInd := strings.LastIndex(_domain, ":")
			if portSepInd != -1 {
				_domain = _domain[:portSepInd]
			}

			ip := net.ParseIP(_domain)
			if ip != nil {
				domain = ip.String()
			} else {
				if this.options.Wildcard {
					parts := strings.Split(_domain, ".")
					if len(parts) >= 2 {
						parts = []string{parts[len(parts)-2], parts[len(parts)-1]}
						_domain = strings.Join(parts, ".")
					}
					domain = "." + _domain
				} else {
					domain = _domain
				}
			}
		}

		cookie := &http.Cookie{
			Name:     this.options.Name,
			Value:    encoded,
			Path:     "/",
			HttpOnly: true,
			Secure:   this.options.Secure,
			Domain:   domain,
			Expires:  time.Now().UTC().Add(age),
		}

		http.SetCookie(w, cookie)
	} else {
		log.Println("Error encoding cookie", err.Error())
	}
}
