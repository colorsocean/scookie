package scookie

import (
	"crypto/rand"
	"fmt"
	"io"
	"regexp"
	"time"
)

var (
	matchUUIDNoDashes = regexp.MustCompile(`^[a-f0-9]{32}$`)
)

func randomUUIDBytes() []byte {
	uuid := make([]byte, 16)
	n, err := io.ReadFull(rand.Reader, uuid)
	if n != len(uuid) || err != nil {
		panic(fmt.Sprintf("RandomUUIDBytes# len:%d, err:%v", n, err))
	}
	uuid[8] = uuid[8]&^0xc0 | 0x80
	uuid[6] = uuid[6]&^0xf0 | 0x40
	return uuid
}

func randomUUIDNoDashes() string {
	uuid := randomUUIDBytes()
	return fmt.Sprintf("%x%x%x%x%x", uuid[0:4], uuid[4:6], uuid[6:8], uuid[8:10], uuid[10:])
}

func maxDuration(ds ...time.Duration) (d time.Duration) {
	d = 0

	for _, d1 := range ds {
		if d1 > d {
			d = d1
		}
	}

	return
}
